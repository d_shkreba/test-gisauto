<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Results
 *
 * @ORM\Table(name="results")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ResultsRepository")
 */
class Results
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="gameuid", type="string", length=255, unique=true)
     */
    private $gameuid;

    /**
     * @var int
     *
     * @ORM\Column(name="steps", type="integer")
     */
    private $steps;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameuid
     *
     * @param string $gameuid
     *
     * @return Results
     */
    public function setGameuid($gameuid)
    {
        $this->gameuid = $gameuid;

        return $this;
    }

    /**
     * Get gameuid
     *
     * @return string
     */
    public function getGameuid()
    {
        return $this->gameuid;
    }

    /**
     * Set steps
     *
     * @param integer $steps
     *
     * @return Results
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;

        return $this;
    }

    /**
     * Get steps
     *
     * @return int
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Results
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
}

