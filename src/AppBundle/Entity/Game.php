<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * game
 *
 * @ORM\Table(name="Game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="game_uid", type="string", length=40, unique=true)
     */
    private $gameUid;

    /**
     * @var string
     *
     * @ORM\Column(name="user_session_id", type="string", length=255, unique=false)
     */
    private $userSessionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime")
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_time", type="datetime", nullable=true)
     */
    private $lastTime;

    /**
     * @var array
     *
     * @ORM\Column(name="state", type="array")
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="steps", type="integer", options={"default":0})
     */
    private $steps;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameUid
     *
     * @param string $gameUid
     *
     * @return game
     */
    public function setGameUid($gameUid)
    {
        $this->gameUid = $gameUid;

        return $this;
    }

    /**
     * Get gameUid
     *
     * @return string
     */
    public function getGameUid()
    {
        return $this->gameUid;
    }

    /**
     * Set userSessionId
     *
     * @param string $userSessionId
     *
     * @return game
     */
    public function setUserSessionId($userSessionId)
    {
        $this->userSessionId = $userSessionId;

        return $this;
    }

    /**
     * Get userSessionId
     *
     * @return string
     */
    public function getUserSessionId()
    {
        return $this->userSessionId;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return game
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set lastTime
     *
     * @param \DateTime $lastTime
     *
     * @return game
     */
    public function setLastTime($lastTime)
    {
        $this->lastTime = $lastTime;

        return $this;
    }

    /**
     * Get lastTime
     *
     * @return string
     */
    public function getLastTime()
    {
        return $this->lastTime;
    }

    /**
     * Set state
     *
     * @param array $state
     *
     * @return game
     */
    public function setState($state)
    {
        $this->state = $state;
        $this->steps++;

        return $this;
    }

    /**
     * Get state
     *
     * @return array
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set steps
     *
     * @param integer $steps
     *
     * @return game
     */
    public function setSteps( $steps )
    {
        $this->steps = $steps;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * Заполняем игру дефолтным состоянием
     */
    public function setDefaultState()
    {
        $this->setState( array_fill(0,5, array_fill(0,5,-1)));
    }
}

