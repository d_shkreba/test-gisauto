<?php
/**
 * Created by PhpStorm.
 * User: HPnettop5
 * Date: 21.09.2017
 * Time: 0:25
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Game;
use AppBundle\Service\GameService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class GameController extends FOSRestController
{

    /**
     * @Rest\Get("api/game/{game_uid}")
     */
    public function getAction( $game_uid )
    {
        $session = new Session();
        if($session->isStarted()) {
            $session->start();
        }
        $activeGame = $this->getDoctrine()->getRepository("AppBundle:Game")->findOneBy(['gameUid' => $game_uid]);
        if(!$activeGame){
            return [];
        }

        return $activeGame;
    }

    /**
     * @Rest\Post("/api/game")
     */
    public function actionCreate()
    {
        $session = new Session();
        if($session->isStarted()) {
            $session->start();
        }

        $activeGame = new Game();
        $activeGame->setUserSessionId($session->getId());
        $activeGame->setGameUid(bin2hex(random_bytes(10)));
        $activeGame->setDefaultState();
        $activeGame->setSteps(0);
        $activeGame->setStartTime(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($activeGame);
        $em->flush();

        return $activeGame;
    }

    /**
     * @Rest\Put("/api/game/{game_uid}")
     */
    public function actionMakeStep(Request $request, $game_uid )
    {
        $data = $request->request->all();

        $game = $this->getDoctrine()->getRepository("AppBundle:Game")->findOneBy(['gameUid' => $game_uid]);
        $oGameService = new GameService($game->getState());
        $oGameService->makeStep( $data['x'], $data['y']);
        $game->setState($oGameService->getState());

        $em = $this->getDoctrine()->getManager();
        $em->persist($game);
        $em->flush();

        return $game;
    }

    /**
     * @Rest\Delete("/api/game/{game_uid}")
     */
    public function actionDelete( $game_uid )
    {
        $this->getDoctrine()->getRepository("AppBundle:Game")->deleteByGuid( $game_uid );
        return ['status' => 'ok'];
    }


}