(function( $ ){
    'use strict';

    /**
     * Класс игры
     */
    class Game{
        constructor ( options ){
            /* Настройки игры */
            this.settings = {};
            /* Количество ходов */
            this.score = 0;
            /* Текущая карта игры */
            this.map = [];
            /* ID игры */
            this.gameId = '';
            /* Состояния для лоадера */
            this.states = {
                loading: 'Подождите, идёт загрузка игры...',
                makeStep: 'Обработка хода'
            };
            /* Онлоад */
            this.onLoad = false;
            /* Выиграли ли */
            this.isWin = false;
        }

        /**
         * Инициализация игры
         * @param options
         * @param callback
         */
        init( options , callback ){
            this.settings = $.extend({
                'container': '#cubes',
                'score': '#score',
                'size': 5,
                'resetButton': '#resetGame',
                'stateBlock': '#state'
            }, options);

            this.__initWinModal();
            this.__initLeadersModal();

            let $this = this;
            $this.onLoad = true;
            $this.showState($this.states.loading);
            if(!localStorage.getItem('gameid')) {
                $.post('/api/game', function (data) {
                    localStorage.setItem('gameid', data.game_uid);
                    $this.gameId = data.game_uid;
                    $this.map = data.state;
                    $this.updateScores( data.steps );
                    callback();
                });
            }else{
                this.gameId = localStorage.getItem('gameid');
                $.get('/api/game/'+this.gameId, function (data) {
                    $this.map = data.state;
                    $this.updateScores( data.steps );
                    callback();
                });
            }

        }

        /**
         * Устанавливает события на элементы, связанные с победой!
         * @private
         */
        __initWinModal()
        {
            $('#userName').click(function(){
                $(this).parent().removeClass('has-warning');
            });

            let $this = this;
            $('#saveWinner').on('click', function( event ){
                let username = $('#userName').val();
                console.log(username);
                if(!$this.isWin){
                    return;
                }
                if( username === '' )
                {
                    $('#userName').parent().addClass('has-warning');
                    return;
                }
                $.post('/api/results',
                    {
                        gameuid: $this.gameId,
                        username: username
                    },
                    function (data) {
                        $('#winModal').modal('hide');
                    }
                );
            });
        }

        /**
         * Устанавливает события на модалку лидеров
         * @private
         */
        __initLeadersModal()
        {
            $('#getLeaders').click(function(){
                $.get('/api/results',function(data){
                    $('#leadersModal').modal();
                    let $table = $('#leadersTable');
                    let pos = 1;
                    data.forEach(function( el ){
                        let element = '<tr>' +
                            '<td>'+pos+'</td>' +
                            '<td>'+el.username+'</td>' +
                            '<td>'+el.steps+'</td>' +
                            '</tr>';

                        $table.find('tbody').append(element);
                        pos++;
                    });
                });
            });

        }

        /**
         * Рисует линию игрового поля
         * @param row
         */
        drawCubesLine(row) {
            for(let i = 0; i < this.settings.size; i++)
            {
                this.__drawCube( i, row );
            }
            this.__breakLine();
        }

        /**
         * Рисует куб
         * @param x
         * @param y
         */
        drawCube( x , y ) {
            let cube = $('<div />', {
                'class': 'cube inactive',
                'data-x': x,
                'data-y': y,
            });
            $(this.settings.container).append( cube );
        }

        /**
         * Рисует clearfix
         * @private
         */
        __breakLine (){
            let el = $('<div />',{
                'class' : 'clearfix'
            });

            $(this.settings.container).append( el );
        }

        /**
         * Обработчик нажатия на куб
         * @param event
         */
        triggerClick( event ){
            if(this.onLoad || this.isWin){
                return;
            }
            let $el = $(event.target);
            let x = $el.data('x');
            let y = $el.data('y');
            let $this = this;
            $this.onLoad = true;
            $this.showState($this.states.makeStep);
            $.ajax({
                url: '/api/game/'+this.gameId,
                type: 'PUT',
                data: {
                    x: x,
                    y: y
                },
                success: function( data ) {
                    $this.onLoad = false;
                    $this.map = data.state;
                    $this.updateScores( data.steps );
                    $this.render();
                    $this.hideState();
                }
            });
        }

        /**
         * Показывает текст в шапке при обработке события
         * @param text
         */
        showState( text )
        {
            $(this.settings.stateBlock).fadeIn('fast').find('span').text(text);
        }

        /**
         * Скрывает текст в шапке
         */
        hideState()
        {
            $(this.settings.stateBlock).fadeOut('fast');
        }

        /**
         * Обновляем игровое поле
         */
        render(){
            if(this.onLoad){
                return;
            }
            let allOn = true;
            for(let y in this.map){
                for(let x in this.map[y]){
                    if(this.map[y][x] > 0){
                        $(this.settings.container).find('.cube[data-y="'+y+'"][data-x="'+x+'"]').removeClass('inactive').addClass('active');
                    }else{
                        allOn = false;
                        $(this.settings.container).find('.cube[data-y="'+y+'"][data-x="'+x+'"]').removeClass('active').addClass('inactive');
                    }
                }
            }

            if(allOn){
                this.isWin = true;
                this.__showSuccessMessage();
            }
        }

        /**
         * Метод показывает модалку победителя
         * @private
         */
        __showSuccessMessage(){
            $(this.settings.resetButton).addClass('animated');
            $('#winModal').modal();
            clearInterval(intervalId);
        }

        /**
         * Навешиваем события на кнопки игрового поля
         * @private
         */
        __bindActions()
        {
            this.onLoad = false;
            this.hideState();
            $(this.settings.container + ' .cube').on('click', function( event ){
                g.triggerClick( event );
            });
            $(this.settings.resetButton).on('click', function( event ){
                g.resetGame( event );
            });
        }

        /**
         * Сбрасывает текущую игру
         * @param event
         */
        resetGame( event )
        {
            $(this.settings.resetButton).removeClass('animated');
            let oldgameId = this.gameId;
            let $this = this;
            $this.showState(this.states.loading);
            $this.onLoad = true;
            // Отправляем запрос на создание новой игры, а затем удаляем старую
            $.post('/api/game', function (data) {
                $this.isWin = false;
                $this.onLoad = false;
                $this.hideState();
                localStorage.setItem('gameid', data.game_uid);
                $this.gameId = data.game_uid;
                $this.map = data.state;
                $this.score = 0;
                $this.render();

                $.ajax({
                    url: '/api/game/'+oldgameId,
                    type: 'DELETE',
                    success: function( data ) {
                        //TODO обработать ошибки
                    }
                });

            });
        }

        /**
         * Показывает количество ходов
         * @param scores
         */
        updateScores( scores )
        {
            this.score = scores;
            $(this.settings.score).text('Количество ходов: '+this.score);
        }
    }

    // Инициализируем игру
    let g = new Game();
    // Антихак
    let intervalId = setInterval(function(){ g.render() }, 5000);

    // Методы плагина
    let methods = {
        init: function( options ) {
            g.init( options , function(){
                for(let i = 0; i < g.settings.size; i++)
                {
                    g.drawCubesLine( i );
                }
                g.bindActions();
            });

        },
        getScore: function() {

        }
    };

    $.fn.game = function( method ){

        if ( methods[method] ) {
            console.log('----'+ method + '----');
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            console.log('----init----');
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует' );
        }
    };

})(jQuery);