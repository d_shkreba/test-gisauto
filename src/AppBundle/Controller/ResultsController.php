<?php
/**
 * Created by PhpStorm.
 * User: HPnettop5
 * Date: 21.09.2017
 * Time: 0:25
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Results;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;

class ResultsController extends FOSRestController
{

    /**
     * @Rest\Get("/api/results")
     */
    public function getResults()
    {
        $results = $this->getDoctrine()->getRepository("AppBundle:Results")->findLeaders();
        if(!$results){
            return [];
        }

        return $results;
    }

    /**
     * @Rest\Post("/api/results")
     */
    public function actionCreate(Request $request)
    {
        $data = $request->request->all();

        if(empty($data['gameuid']) || empty($data['username']))
        {
            return new View('Не переданы обязательные параметры');
        }

        $game = $this->getDoctrine()->getRepository("AppBundle:Game")->findOneBy(['gameUid' => $data['gameuid']]);
        if(!$game){
            return new View('Игры с таким ID не существует');
        }

        $result = new Results();
        $result->setGameuid($data['gameuid']);
        $result->setSteps($game->getSteps());
        $result->setUsername($data['username']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($result);
        $em->flush();

        return $result;
    }


}