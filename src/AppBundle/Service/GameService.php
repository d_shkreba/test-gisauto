<?php
/**
 * Created by PhpStorm.
 * User: shkreba
 * Date: 22.09.2017
 * Time: 17:15
 */

namespace AppBundle\Service;


use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Класс для обработки ходов игры
 * Class GameService
 * @package AppBundle\Service
 */
class GameService
{
    /** @var  array  */
    private $_state;
    /** @var int Размерность поля */
    private $_dimension = 5;

    public function __construct( Array $state )
    {
        $this->_state = $state;
    }

    /**
     * Обработка хода
     * @param int $x
     * @param int $y
     */
    public function makeStep(int $x, int $y)
    {
        $this->validatePosition($x);
        $this->validatePosition($y);

        $startX = ($x == 0)?$x:$x-1;
        $endX = ($x == $this->_dimension - 1)? $x : $x+1;

        $startY = ($y == 0)?$y:$y-1;
        $endY = ($y == $this->_dimension - 1)? $y : $y+1;

        for($i = $startY; $i <= $endY; $i++)
        {
            for($j = $startX; $j <= $endX; $j++)
                $this->_state[$i][$j] = -$this->_state[$i][$j];
        }
    }

    /**
     * Проверка позиции
     * @param $pos
     */
    protected function validatePosition( $pos )
    {
        if($pos < 0 || $pos > $this->_dimension - 1)
        {
            throw new Exception('Position is out of range');
        }
    }


    public function getState()
    {
        return $this->_state;
    }


}