<?php

namespace AppBundle\Repository;

/**
 * ResultsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ResultsRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * Метод показывает 10 топ игроков
     * @return array
     */
    public function findLeaders()
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('r')
            ->from('AppBundle:Results', 'r')
            ->orderBy('r.steps', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

}
